module.exports = {
  "Name": "NumberFormat",
  "Repository": "git@bitbucket.org:jasonvillalon/number-format.git",
  "AtomicDeps": [],
  "dependencies": {
    "numeral": "^*",
    "config": "^*"
  },
  "config": {
    "currencySymbol": "$"
  }
};
