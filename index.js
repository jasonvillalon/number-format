import config from "config"
import numeral from "numeral"
// console.log(numeral)
numeral.register("locale", "default", {
  delimiters: {
    thousands: ", ",
    decimal: "."
  },
  abbreviations: {
    thousand: "k",
    million: "m",
    billion: "b",
    trillion: "t"
  },
  ordinal : function(number) {
    return number === 1 ? "er" : "ème"
  },
  currency: {
    symbol: config.get("currencySymbol")
  }
})

// switch between languages
numeral.locale("default")

export function format(amount, f = "$0,0.00") {
  let string = numeral(amount).format(f)
  return string
}

export function unformat(amount) {
  return numeral(amount).value()
}

export default {
  format,
  unformat
}
